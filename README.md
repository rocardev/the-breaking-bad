# The Breaking Bad
Author: Robert Cárdenas
##### Challenge: Technical challenge: Test project for atSistemas

## Introduction:
This project shows information about all the characters from the television series "The Breaking Bad". On the main screen you will see a list of all the characters. this view shows a search engine so that you can filter the character you want by name. Once you click on any of the characters, you will see a new screen where more detailed information about the character is displayed.
In addition, a famous phrase of the character is shown, used during the series. If the selected character has more than one phrase, you will be able to see all his phrases by clicking on the link shown on this screen.

## Development:
The packages used for the project are listed below:

- **React, Webpack and Babel**:
    For this project I did not use the Facebook CLI "create-react-app" to create my project, I preferred to install React, Webpack and Babel separately.

- **Axios**:
    It's a Javascript library used to make HTTP requests.

- **Bootswatch**:
    In the project is applied Bootswatch, it's based on Boostrap, a potent front-end framework used to create modern websites and web apps.

- **i18next**:
    It's an internationalization-framework written in and for JavaScript in order to manage localization and language in our applications.

- **React-Redux**
    Redux is used mostly for application state management.
   
    *Note that Redux was required for this technical test and I applied it in the project, but I have also used the local state in some components using **hooks**, this is in order to show the different capacities or ways of working, all adapted according to the needs of the project and the company.*


## Instalation:

Once you clone the repository in your local, you can run:

```javascript
$ npm install
$ npm start
```

Author: **Robert Cárdenas Díaz**

You can see the production URL:
http://thebreakingbad.surge.sh/

Enjoy it.


