import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import logo from "../img/logo.png";

const Header = ({language, toggleLanguage}) => {
  const [t, i18n] = useTranslation("global");
  useEffect(() => {
    i18n.changeLanguage(language)
  }, [language])

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-3">
            <Link className="navbar-brand" to="/">
              <img className="img-logo" src={logo} alt="" />
            </Link>
          </div>
          <div className="col-md-6">
            <h3 className="header-title">{t("header_title")}</h3>
          </div>
          <div className="col-md-3">
            <button
              id="lang-en"
              type="button"
              className={language==='en' ? 'btn-lang active' : 'btn-lang'}
              onClick={() => {toggleLanguage('en')}}              
            >
              EN
            </button>
            <button
              id="lang-es"
              type="button"
              className="btn-lang"
              className={language==='es' ? 'btn-lang active' : 'btn-lang'}
              onClick={() => {toggleLanguage('es')}}
            >
              ES
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = state => {
  return {language: state.language}
}

const mapDispatchToProps = dispatch => ({
  toggleLanguage(lang) {
      dispatch({
          type: "CHANGE_LANGUAGE",
          lang
      })
  }
})
export default connect(mapStateToProps, mapDispatchToProps)(Header)
