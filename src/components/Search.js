import React, { useState } from 'react'
import { useTranslation } from "react-i18next";

const Search = ({textToFind}) => {
    const [t, i18n] = useTranslation("global")
    const [error, setError] = useState('')
    const onChangeInput = (txt) => {
        textToFind(txt)
        setError('')
    }
    return (
            <div>
                <input 
                    className="form-control me-sm-2" 
                    type="text" 
                    id="searchInput"
                    placeholder={t("search_input")}
                    onChange={(e) => onChangeInput(e.target.value)}
                    autoFocus/>
                <p>{error ? error : ''}</p>
            </div>
        )
    }
export default Search