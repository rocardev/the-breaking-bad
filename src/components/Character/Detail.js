import React, {useState, useEffect} from 'react'
import { Link, useParams } from "react-router-dom";
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import axios from 'axios'
import {API_QUOTES} from '../api'
import Header from '../Header'
import Spinner from '../Spinner/Spinner'
import Error from '../Error/Error'
import QuoteDetail from './QuoteDetail';

const Detail = ({characters, quotes, saveData, quote}) => {
    const [t, i18n] = useTranslation("global")
    const [isLoading, setIsLoading] = useState(false)
    const [apiConnError, setApiConnError] = useState('')
    const { name } = useParams();
    let author=name
    const API = `${API_QUOTES}?author=${author}`
    const character = characters.filter(c => c.name === name.replaceAll('+',' '))[0]

    const getQuotes = async () => {
        try {
            const res = await axios.get(API)
            if (Array.isArray(res.data)) {
                if (res.data.length >= 1 ) {
                    setApiConnError('')
                    saveData(res.data)
                    setIsLoading(false)
                } else {
                    setApiConnError('')
                    saveData([])
                    setIsLoading(false)
                }
            }  else {
                setApiConnError('error')
            }
        } catch (error) {
            saveData([])
            setIsLoading(false)
            setApiConnError('error')
        }
    }
    useEffect(() => {
        getQuotes()
    },[quote])
if (apiConnError==='error') {
    return (<div className='container'>
        <Header />
        <Error />
    </div>)
} else if (isLoading === true) {
    return (
        <div className='container'>
            <Header />
            <Spinner />
        </div>
    )
} else {
    return (
        <div className='container'>
            <Header />
            <div className="card mb-3">
                <h3 className="card-header">{t("character_info_title")}</h3>
                <Link className="navbar-brand" to="/">
                    <button type="button" className="btn-back">{t("character_info_back_button")}</button>
                </Link>
                <div className="card-body">
                    <h5 className="card-title">{character.name}</h5>
                    <h6 className="card-subtitle text-muted">{character.nickname}</h6>
                </div>
                <div className="row" >
                    <div className="col-md-4">
                        <img
                            src={character.img}
                            alt={character.name}
                            id={character.char_id}
                            className="imgInfoCharacter"
                        />
                    </div>
                    <div className="col-md-8">
                        <div className="card-body">
                            <p className="card-text">{t("character_info_main_description")} {character.name}:</p>
                        </div>
                        <ul className="list-group list-group-flush">
                            {character.birthday !== null && character.birthday !== 'Unknown' &&
                                <li className="list-group-item"><span className="info-properties">{t("character_info_birthday")}: </span>{character.birthday}</li>
                            }
                            <li className="list-group-item"><span className="info-properties">{t("character_info_category")}: </span> {character.category}</li>
                            <li className="list-group-item"><span className="info-properties">{t("character_info_ocupation")}: </span> {character.occupation}</li>
                            <li className="list-group-item"><span className="info-properties">{t("character_info_status")}: </span> {character.status}</li>
                            <li className="list-group-item"><span className="info-properties">{t("character_info_who_played")} </span> {character.portrayed}</li>
                            {character.appearance != undefined &&
                                <li className="list-group-item"><span className="info-properties">Appearance in seasons:</span> {character.appearance.join()}</li>
                            }
                            {quotes.length>=1 &&
                            <QuoteDetail quotes={quotes}/>}
                        </ul>
                    </div>
                </div>
            </div>
        </div>)
    }
}

const mapStateToProps = state => {
    return {
        characters: state.characters,
        quotes: state.quotes,
        quote: state.quote
    }
}
  
const mapDispatchToProps = dispatch => ({
    saveData(quotes) {
        dispatch({
            type: "SAVE_QUOTES",
            quotes
        })
    },
})
export default connect(mapStateToProps, mapDispatchToProps)(Detail)
