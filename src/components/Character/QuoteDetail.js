import React, { useState, useEffect } from 'react';
import { useTranslation } from "react-i18next";

export default function QuoteDetail({quotes}) {
    const [t, i18n] = useTranslation("global")
    const [currentQuote, setCurrentQuote] = useState('')

    const updateQuote = () => {
        if (quotes.length != 0) {
            if (quotes.length == 1) {
                setCurrentQuote(quotes[0].quote)
            } else {
                let randomItem = Math.floor(Math.random()*quotes.length);
                setCurrentQuote(quotes[randomItem].quote)
            }
        }
    }
    useEffect(() => {
        updateQuote()
    }, [])
    return (
        <>
            <li className="list-group-item"><span className="info-properties">{t("character_info_famous_phrase")}: </span>
                <p className="phrase">{currentQuote}</p>
                 {quotes !== undefined && quotes.length >1 && 
                    <p className="phrase">{t("character_info_another_prase_question")} <a href="#" className="card-link" onClick={() => {updateQuote()}}>{t("character_info_another_prase_link")}</a></p>}
            </li>
        </>
    )
}
