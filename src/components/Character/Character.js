import React from 'react'
import { Link } from 'react-router-dom'
const Character = ({character}) => {
    return (
        <>
            <div className="col-md-3">
                <Link to={"/detail/"+character.name.replaceAll(' ','+')}>
                    <img 
                        src={character.img} 
                        alt={character.name} 
                        id={character.char_id}
                        className="img-fluid rounded"
                        data-bs-toggle="tooltip" 
                        data-bs-placement="top" 
                        title={character.nickname}
                    />
                </Link>
                <div className="card-body">
                    <p className="info-properties">{character.name}</p>
                </div>
                
            </div>
        </>
    )
}
export default Character