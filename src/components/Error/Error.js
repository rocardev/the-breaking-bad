import React from 'react'
import { useTranslation } from "react-i18next";
import './styles.css'

const Error = () => {
    const [t, i18n] = useTranslation("global")
    return <div>
            <div className="errorMessge">
                {t("app_message_connection_error")}
            </div>
            <div className="img-error" ></div>
            
        </div>

}

export default Error