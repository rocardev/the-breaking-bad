import React from 'react'
import { useTranslation } from "react-i18next";
import './styles.css'

export default function Spinner () {
    const [t, i18n] = useTranslation("global")
  return (
        <>
            <div className="loading">{t("app_message_loading")}
                <span-loading></span-loading>
            </div>
            
        </>
    )
}