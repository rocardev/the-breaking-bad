import React, {useEffect, useState} from 'react'
import { connect } from "react-redux";
import { useTranslation } from "react-i18next";
import axios from 'axios'
import {API_CHARACTERS} from './api'
import Header from './Header'
import Search from './Search'
import Character from './Character/Character'
import Spinner from './Spinner/Spinner'
import Error from './Error/Error'
import '../styles/styles.css'
import 'bootswatch/dist/slate/bootstrap.min.css'

const Main = ({characters, saveData}) => {
    const [t, i18n] = useTranslation("global")
    const [isLoading, setIsLoading] = useState(true)
    const [apiConnError, setApiConnError] = useState('')
    const [nameToSearch, setnameToSearch] = useState('')
    const API = `${API_CHARACTERS}?name=${nameToSearch}`
   
    const getCharactersFromAPI = async () => {
        try {
            const res = await axios.get(API, {timeout: 5000})
            if (Array.isArray(res.data)) {
                setIsLoading(false)
                saveData(res.data)
            }  else {
                setnameToSearch('no-data')
                setApiConnError('error')
            }
        }
        catch (error) {
            setApiConnError('error')
        }
    }
    useEffect(() => {
        getCharactersFromAPI();
    }, [nameToSearch]);
    return (
        <div className='container'>
            <Header />
            {apiConnError === 'error' ?
                <Error />
            :
                isLoading ?
                    <Spinner />
                :
                    <>
                        <Search textToFind={(e) => setnameToSearch(e)} />
                        {characters.length>0 && 
                            <div className="row" >
                                {characters.map(character => {
                                    return (
                                        <Character 
                                            key={character.char_id}
                                            character={character} 
                                        />
                                    )
                                })
                                }
                            </div>
                        }
                    </>
                }
        </div>
    ) 
}
const mapStateToProps = state => {
    return {characters: state.characters}
  }
  
  const mapDispatchToProps = dispatch => ({
    saveData(characters) {
        dispatch({
            type: "SAVE_CHARACTERS",
            characters
        })
    }
  })
export default connect(mapStateToProps, mapDispatchToProps)(Main)
