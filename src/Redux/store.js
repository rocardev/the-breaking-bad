import { createStore } from "redux"

export const initialState = {
    language: 'en',
    characters: [],
    quotes: [],
    quote: '',
}
const reducerStore = (state = initialState, action) => {
    switch (action.type) {
        case 'SAVE_CHARACTERS':
            return {...state, characters: action.characters}
        case 'CHANGE_LANGUAGE':
            return {...state, language: action.lang}
        case 'SAVE_QUOTES':
            return {...state, quotes:action.quotes}
        case 'GET_CURRENT_QUOTE':
            return {...state, quote: action.quote
            }
        default:
            return state;
    }
    return state
}

export default createStore(
    reducerStore,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)