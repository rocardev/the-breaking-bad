import React from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom'
import { Provider} from 'react-redux'
import Main from './components/Main'
import Detail from './components/Character/Detail'
import store from './Redux/store'

function App() {
  return (
    <Provider store={store}>
    <Router>
        <Route path="/" exact component={Main}/>
        <Route path="/detail/:name" component={Detail}/>
    </Router>
    </Provider>
  );
}

export default App;
