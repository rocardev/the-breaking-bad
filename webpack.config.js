const HtmlwebpackPlugin = require('html-webpack-plugin')

const htmlPlugin = new HtmlwebpackPlugin({
    template: './src/index.html',
    filename: './index.html'
})
module.exports = {
    module: {
        //Se crean reglas para que webpack sepa qué hacer con los archivos que son distintos a JavaScript
        rules: [
            
            {
                test: /\.js$/,
                exclude: /node_modules/,
                /*use: ["babel-loader", "eslint-loader"]   //Babel-loader necesita su propia configuración pero lo creamos en un fichero aparte: .babelrc*/
                use: ["babel-loader"]   //Babel-loader necesita su propia configuración pero lo creamos en un fichero aparte: .babelrc
            },
            {
                test: /\.css$/,
                use: ['style-loader','css-loader']  //Este arreglo se ejecuta de derecha a izquierada 
            },
            {
                type: "asset",
                test: /\.(png|svgjpg|jpeg|gif)$/i,
            }
        ]
    },
    plugins: [htmlPlugin],
    devServer: {
        historyApiFallback: true,
      },
}
